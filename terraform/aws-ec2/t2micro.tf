
variable"instance_type"{}
variable"access_key"{}
variable"secret_key"{}

provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "us-east-2"
}

resource "aws_instance" "ec2" {
  ami           = "ami-cf172aaa"
  instance_type = "${var.instance_type}"

}
