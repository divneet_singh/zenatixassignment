provider "google" {
  credentials = "${file("account.json")}"
  project     = "gentle-operator-205103"
  region      = "us-east1"
  }

resource "google_compute_instance" "default" {
  name         = "test"
  machine_type = "n1-standard-1"
  zone         = "us-east1-b"


boot_disk {
    initialize_params {
      image = "ubuntu-1604-xenial-v20180522"

    }
  }

network_interface {
    network = "default"
    access_config {
      // Ephemeral IP
    }

  }

service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }
}
